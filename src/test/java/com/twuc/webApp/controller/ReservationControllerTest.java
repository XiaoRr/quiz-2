package com.twuc.webApp.controller;

import com.twuc.webApp.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ReservationControllerTest extends ApiTestBase {

    @Test
    void add_reservations() throws Exception {
        mockMvc.perform(post("/api/staffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));
         mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "  \"username\": \"Sofia\",\n" +
                        "  \"companyName\": \"ThoughtWorks\",\n" +
                        "  \"zoneId\": \"Africa/Nairobi\",\n" +
                        "  \"startTime\": \"2019-08-20T11:46:00+03:00\",\n" +
                        "  \"duration\": \"PT1H\"\n" +
                        "}")
        ).andExpect(status().isOk());
    }


}