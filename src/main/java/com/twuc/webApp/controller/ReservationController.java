package com.twuc.webApp.controller;

import com.twuc.webApp.entity.Reservation;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.repository.ReservationRepository;
import com.twuc.webApp.repository.StaffRepository;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.format.annotation.DateTimeFormat.*;

@RestController
public class ReservationController {

    private final StaffRepository staffRepository;

    private final ReservationRepository reservationRepository;

    public ReservationController(StaffRepository staffRepository, ReservationRepository reservationRepository) {
        this.staffRepository = staffRepository;
        this.reservationRepository = reservationRepository;
    }

    @PostMapping("/api/staffs/{staffId}/reservations")
    public ResponseEntity addReservation(@PathVariable Long staffId, @Valid @RequestBody @DateTimeFormat(iso = ISO.DATE_TIME) Reservation reservation){
        Optional<Staff> staff = staffRepository.findById(staffId);
        if(staff.isEmpty()){
            return ResponseEntity.status(400).build();
        }
        reservationRepository.saveAndFlush(reservation);
        return ResponseEntity.status(200)
                .header("Location","/api/staffs/"+staffId+"/reservations").build();
    }

    @GetMapping("/api/staffs/{staffId}/reservations")
    public ResponseEntity getReservation(@PathVariable Long staffId){
        Optional<Staff> staff = staffRepository.findById(staffId);
        if(staff.isEmpty()){
            return ResponseEntity.status(400).build();
        }
        return ResponseEntity.status(200).build();
    }

}
