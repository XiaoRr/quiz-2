package com.twuc.webApp.controller;


import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.entity.ZoneId;
import com.twuc.webApp.repository.StaffRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class StaffController {

    private StaffRepository staffRepository;

    public StaffController(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }

    @PostMapping("/api/staffs")
    public ResponseEntity addStaff(@Valid @RequestBody Staff staff){
        Staff savedStaff = staffRepository.save(staff);
        staffRepository.flush();
        return ResponseEntity.status(200).header("Location","/api/staffs/"+savedStaff.getId()).build();
    }

    @GetMapping("api/staffs/{id}")
    public ResponseEntity getStaff(@PathVariable Long id){
        Optional<Staff> staff = staffRepository.findById(id);
        if(staff.isEmpty()){
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200).body(staff);
    }

    @GetMapping("api/staffs")
    public ResponseEntity getStaffs(){
        return ResponseEntity.status(200).body(staffRepository.findAll());
    }

    @PutMapping(value = "/api/staffs/{staffId}/timezone")
    public ResponseEntity setStaffTimeZone(@PathVariable Long staffId,@Valid @RequestBody ZoneId zoneId){
        Optional<Staff> staff = staffRepository.findById(staffId);
        if(staff.isEmpty()){
            return ResponseEntity.status(400).build();
        }
        staff.get().setZoneId(zoneId.getZoneId());
        staffRepository.saveAndFlush(staff.get());
        return ResponseEntity.status(200).build();
    }
}
