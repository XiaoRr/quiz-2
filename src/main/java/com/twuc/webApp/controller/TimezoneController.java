package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.zone.ZoneRulesProvider;

@RestController
public class TimezoneController {

    @GetMapping("/api/timezones")
    public ResponseEntity getTimeZoneList(){
        return ResponseEntity.status(200).body(ZoneRulesProvider.getAvailableZoneIds());
    }
}
