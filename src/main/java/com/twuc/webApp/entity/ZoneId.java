package com.twuc.webApp.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class ZoneId {
    @NotNull
    @JsonProperty(value = "zoneId")
    private String zoneId;

    public ZoneId() {
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
