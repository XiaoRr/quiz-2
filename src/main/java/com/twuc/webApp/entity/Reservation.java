package com.twuc.webApp.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@Entity
public class Reservation {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    @Size(max = 128)
    @NotNull
    private String username;

    @Column
    @Size(max = 64)
    @NotNull
    private String companyName;

    @NotNull
    private String zoneId;

    @NotNull
    private ZonedDateTime startTime;

    @NotNull
    @Pattern(regexp="^PT[1-3]H$")
    private String duration;

    @ManyToOne
    private Staff staff;

    public Reservation() {
    }

    public Reservation(@Size(max = 128) @NotNull String username, @Size(max = 64) @NotNull String companyName, @NotNull String zoneId, ZonedDateTime startTime, @NotNull @Pattern(regexp = "^PT[1-3]H$") String duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
