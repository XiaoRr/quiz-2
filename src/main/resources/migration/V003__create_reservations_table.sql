CREATE TABLE IF NOT EXISTS reservation (
     `id`               BIGINT              AUTO_INCREMENT PRIMARY KEY,
     `user_name`        VARCHAR(128)        NOT NULL,
     `company_name`     VARCHAR(64)         NOT NULL,
     `start_time`       TIMESTAMP           NOT NULL,
     `duration`         VARCHAR(8)          NOT NULL,
     `zone_id`          VARCHAR(255)        NOT NULL,
     `staff_id`         BIGINT              NOT NULL,
     FOREIGN KEY (staff_id) REFERENCES staff(id)
)
